package sentry

import (
	"github.com/sirupsen/logrus"
)

type LoggerOption func()

func SetupLogs(logLevel string, options ...LoggerOption) {
	level, err := logrus.ParseLevel(logLevel)
	if err != nil {
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)

	for _, o := range options {
		o()
	}
}
