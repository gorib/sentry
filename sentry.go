package sentry

import (
	"github.com/getsentry/sentry-go"
	"github.com/makasim/sentryhook"
	"github.com/sirupsen/logrus"

	"gitlab.com/gorib/session"
	"gitlab.com/gorib/try"
)

func NewSentryLogger(dsn, env string, levels ...logrus.Level) LoggerOption {
	return func() {
		if dsn == "" {
			return
		}
		if len(levels) == 0 {
			levels = []logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel}
		}
		try.ThrowError(sentry.Init(sentry.ClientOptions{Dsn: dsn, Environment: env}))
		logrus.AddHook(sentryhook.New(levels, sentryhook.WithConverter(func(entry *logrus.Entry, event *sentry.Event, hub *sentry.Hub) {
			sentryhook.DefaultConverter(entry, event, hub)
			event.Threads = []sentry.Thread{
				{
					Stacktrace: sentry.NewStacktrace(),
					Crashed:    false,
					Current:    true,
				},
			}

			if sessionContext := session.FromContext(entry.Context); sessionContext != nil {
				event.Tags["correlation_id"] = sessionContext.CorrelationId
				event.Request = &sentry.Request{
					URL:    sessionContext.Uri,
					Method: sessionContext.Method,
					Data:   sessionContext.Body,
				}
			}
		})))
	}
}
