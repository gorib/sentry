package sentry

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestLogs(t *testing.T) {
	t.Run("Error", func(t *testing.T) {
		SetupLogs("error")
		assert.Equal(t, logrus.ErrorLevel, logrus.StandardLogger().Level)

	})

	t.Run("Fallback", func(t *testing.T) {
		SetupLogs("something")
		assert.Equal(t, logrus.InfoLevel, logrus.StandardLogger().Level)
	})
}
