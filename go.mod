module gitlab.com/gorib/sentry

go 1.21.0

require (
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/makasim/sentryhook v0.4.2
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.0
	gitlab.com/gorib/try v0.0.0-20231015095723-28e5c3531383
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/getsentry/sentry-go v0.17.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/gorib/session v0.0.0-20231016141858-5c6a00e5239f // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
